import React, { Component } from "react";
import { View, StyleSheet, Image, Text, TouchableOpacity } from "react-native";
import cronImg from "./src/image/cronometro.png";
class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            numero: 0,
            playPauseBtn: "GO",
        };

        this.timer = null;

        this.go = this.go.bind(this);
        this.stop = this.stop.bind(this);
    }

    go() {
        if (this.timer !== null) {
            clearInterval(this.timer);
            this.timer = null;
            this.setState({ playPauseBtn: "Play" });
        } else {
            this.setState({ playPauseBtn: "Pause" });
            this.timer = setInterval(() => {
                this.setState({ numero: this.state.numero + 0.1 });
            }, 100);
        }
    }

    stop() {
        clearInterval(this.timer);
        this.timer = null;
        this.setState({ numero: 0, playPauseBtn: "GO" });
    }
    render() {
        return (
            <View style={styles.container}>
                <Image source={cronImg} style={styles.cronImg} />
                <Text style={styles.timer}>
                    {this.state.numero.toFixed(1)}{" "}
                </Text>

                <View style={styles.btnArea}>
                    <TouchableOpacity style={styles.btn} onPress={this.go}>
                        <Text style={styles.btnText}>
                            {this.state.playPauseBtn}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={this.stop}>
                        <Text style={styles.btnText}>STOP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#00aeef",
    },

    timer: {
        marginTop: -160,
        color: "#fff",
        fontSize: 65,
        fontWeight: "bold",
    },

    btnArea: {
        flexDirection: "row",
        marginTop: 90,
        height: 40,
    },

    btn: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
        height: 40,
        margin: 17,
        borderRadius: 9,
    },

    btnText: {
        fontSize: 20,
        color: "#00aeef",
        fontWeight: "bold",
    },
});

export default App;
